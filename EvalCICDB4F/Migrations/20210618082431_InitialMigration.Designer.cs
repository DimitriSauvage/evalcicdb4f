﻿// <auto-generated />
using System;
using EvalCICDB4F;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EvalCICDB4F.Migrations
{
    [DbContext(typeof(EvalCICDB4FDbContext))]
    [Migration("20210618082431_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.16")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EvalCICDB4F.Models.WeatherForecast", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<string>("Summary")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TemperatureC")
                        .HasColumnType("int");

                    b.HasKey("Id")
                        .HasName("PK_WeatherForecast_ID");

                    b.ToTable("WeatherForecast");

                    b.HasData(
                        new
                        {
                            Id = new Guid("fe8fe114-1c02-43e3-8095-3e6c4049f5c2"),
                            Date = new DateTime(2021, 6, 10, 10, 0, 0, 0, DateTimeKind.Unspecified),
                            Summary = "Temps nuageux",
                            TemperatureC = 23
                        },
                        new
                        {
                            Id = new Guid("d328b2b2-51d0-4d6a-a20d-72626805b2c9"),
                            Date = new DateTime(2021, 6, 10, 11, 0, 0, 0, DateTimeKind.Unspecified),
                            Summary = "Temps pluvieux",
                            TemperatureC = 25
                        },
                        new
                        {
                            Id = new Guid("3056ab12-0359-4c50-8a8c-76502270efdd"),
                            Date = new DateTime(2021, 6, 10, 15, 0, 0, 0, DateTimeKind.Unspecified),
                            Summary = "Grand soleil",
                            TemperatureC = 35
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
