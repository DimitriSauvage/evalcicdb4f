﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EvalCICDB4F.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WeatherForecast",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    TemperatureC = table.Column<int>(nullable: false),
                    Summary = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeatherForecast_ID", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "WeatherForecast",
                columns: new[] { "Id", "Date", "Summary", "TemperatureC" },
                values: new object[] { new Guid("fe8fe114-1c02-43e3-8095-3e6c4049f5c2"), new DateTime(2021, 6, 10, 10, 0, 0, 0, DateTimeKind.Unspecified), "Temps nuageux", 23 });

            migrationBuilder.InsertData(
                table: "WeatherForecast",
                columns: new[] { "Id", "Date", "Summary", "TemperatureC" },
                values: new object[] { new Guid("d328b2b2-51d0-4d6a-a20d-72626805b2c9"), new DateTime(2021, 6, 10, 11, 0, 0, 0, DateTimeKind.Unspecified), "Temps pluvieux", 25 });

            migrationBuilder.InsertData(
                table: "WeatherForecast",
                columns: new[] { "Id", "Date", "Summary", "TemperatureC" },
                values: new object[] { new Guid("3056ab12-0359-4c50-8a8c-76502270efdd"), new DateTime(2021, 6, 10, 15, 0, 0, 0, DateTimeKind.Unspecified), "Grand soleil", 35 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WeatherForecast");
        }
    }
}
