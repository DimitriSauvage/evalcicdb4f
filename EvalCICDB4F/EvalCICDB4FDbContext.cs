﻿using EvalCICDB4F.Maps;
using Microsoft.EntityFrameworkCore;

namespace EvalCICDB4F
{
    public class EvalCICDB4FDbContext : DbContext
    {
        public EvalCICDB4FDbContext()
        {
        }

        public EvalCICDB4FDbContext(DbContextOptions options) : base(options)
        {
        }


        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Apply configuration
            modelBuilder.ApplyConfiguration(new WeatherForecastMap());
        }

    }
}