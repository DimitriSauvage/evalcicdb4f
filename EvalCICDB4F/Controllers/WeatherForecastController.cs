﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EvalCICDB4F.Models;
using EvalCICDB4F.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EvalCICDB4F.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        public WeatherForecastRepository WeatherForecastRepository { get; }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public WeatherForecastController(WeatherForecastRepository weatherForecastRepository)
        {
            WeatherForecastRepository = weatherForecastRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> GetAsync()
        {
            return await WeatherForecastRepository.GetWeatherForecastsAsync();
        }
    }
}