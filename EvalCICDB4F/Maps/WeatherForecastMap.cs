﻿using System;
using EvalCICDB4F.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EvalCICDB4F.Maps
{
    public class WeatherForecastMap : IEntityTypeConfiguration<WeatherForecast>
    {
        public void Configure(EntityTypeBuilder<WeatherForecast> builder)
        {
            builder.HasKey(x => x.Id).HasName("PK_WeatherForecast_ID");
            builder.Property(x => x.Date);
            builder.Property(x => x.Summary);
            builder.Property(x => x.TemperatureC);

            builder.HasData(
                new WeatherForecast()
                {
                    Date = new DateTime(2021, 06, 10, 10, 0, 0),
                    Id = Guid.NewGuid(),
                    Summary = "Temps nuageux",
                    TemperatureC = 23
                },
                new WeatherForecast()
                {
                    Date = new DateTime(2021, 06, 10, 11, 0, 0),
                    Id = Guid.NewGuid(),
                    Summary = "Temps pluvieux",
                    TemperatureC = 25
                },
                new WeatherForecast()
                {
                    Date = new DateTime(2021, 06, 10, 15, 0, 0),
                    Id = Guid.NewGuid(),
                    Summary = "Grand soleil",
                    TemperatureC = 35
                }
            );
        }
    }
}