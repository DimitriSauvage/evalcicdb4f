﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EvalCICDB4F.Models;
using Microsoft.EntityFrameworkCore;

namespace EvalCICDB4F.Repositories
{
    public class WeatherForecastRepository
    {
        public DbSet<WeatherForecast> WeatherForecasts { get; }

        public WeatherForecastRepository(DbContext dbContext)
        {
            WeatherForecasts = dbContext.Set<WeatherForecast>();
        }

        public async Task<IEnumerable<WeatherForecast>> GetWeatherForecastsAsync()
        {
            return await WeatherForecasts.ToListAsync();
        }
    }
}